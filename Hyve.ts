import express, {Application} from 'express';
import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';
import morgan from 'morgan';
import mongoose from 'mongoose';
import Controller from '@/interfaces/controller.interface';
import ErrorMiddleware from '@/middleware/error.middleware';

/**
 * The main API class / object, where we can monitor, kill, and restart the server as needed.
 *
 * Essentially a *very* basic main function using this class would look something like this:
 * ```js
 * import controllers from '@/controllers/whatever'
 * (() => {
 *    // Call this FIRST so we know that all the variables in our dotenv are all correct.
 *    validateEnv();
 *    // Create the `Hyve` object where we put in our controllers into the array.
 *    const hyve = new Hyve([controllers], Number(process.env.PORT));
 *    // And *then* finally, we can start accepting new connections! And everything *should* run perfectly!
 *    hyve.listen();
 *  })
 * ```
 * @param controllers {Controller[]} - Array of API controllers that we want to monitor and use in our application.
 * @param port {number} - The listening port that we should use. Overrides the default in the dotenv config.
 */
export default class Hyve {
	public server: Application;
	public readonly port: number;

	constructor(controllers: Controller[], port: number) {
		this.server = express();
		this.port = port;

		this.initialiseDatabaseConnection();
		this.initialiseMiddleware();
		this.initialiseControllers(controllers);
		this.initialiseErrorHandling();
	}

	/**
   * Raise middleware interfaces that we use for error catching & request manipulation.
   * This is the function where we also load our security measures such as:
   *
   * - *Helmet*, which provides us things like XSS protection,
   * - *Morgan*, our logging library, so we can monitor what calls are being made to our API, when, and by who
   * - And finally, *Compression*, which compresses the requests made to the API to reduce bandwith & memory storage space.
	 * @returns
  */
	private initialiseMiddleware(): void {
		this.server.use(helmet());
		this.server.use(cors());
		this.server.use(morgan('dev'));
		this.server.use(express.json());
		this.server.use(express.urlencoded());
		this.server.use(compression());
	}

	/**
   * Raise all the controller interfaces the API will use.
   * This is also where we set our base URL that the API will use on the application, for example,
   * in our frontend we might say:
   * ```js
   * fetch(`/api/v1/user/${id}`, userData);
   * ```
  */
	private initialiseControllers(controllers: Controller[]): void {
		for (const controller of controllers) {
			this.server.use('/api/v1', controller.router);
		}
	}

	/**
   * Initialise the error handling modules defined in `/middleware/error.middleware.ts` .
   * This is done to ensure that we have *actually useful errors* when something in the API messes up.
   * And naturally, it all gets logged by *Morgan~*
   */
	private initialiseErrorHandling(): void {
		this.server.use(ErrorMiddleware);
	}

	/**
   * Connect our API process to MongoDB using the dotenv defaults.
   * Note that this calls from our `process.env` directly rather than using the sanitised output from `cleanEnv`,
   * which is why that function should **always** be called on startup **before** initialising the connection.
  */
	private initialiseDatabaseConnection(): void {
		const {MONGODB_USER, MONGODB_PASSWORD, MONGODB_PATH} = process.env;
    mongoose.connect(
      `mongodb://${MONGODB_USER}:${MONGODB_PASSWORD}${MONGODB_PATH}`
    );
		// Alternative means of connecting
		// mongoose.connect(require('node:process').env.MONGODB_URI);
		console.log('MongoDB is connected! Hooray!');
	}

	/**
   * Start the express listening server. Essentialy our `main` function so we can start processing requests.
   * Since we call `this` to find the port, we're using the sanitised output from our `cleanEnv` function, and thus, we
   * always run this function at the **very end** of whatever `main` function we define.
  */
	public listen(): void {
		this.server.listen(this.port, () => {
			console.log(`And we're live! Listening on http://localhost:${this.port}!`);
		});
	}
}

/** There I fucking documented it all unlike you mister @abhinavp06 - I Fucking Hate You Actually Forcing Me To Write Code Because Yours Is Literally Not Documented And Doesn't Work God I Hate You DIE. */
