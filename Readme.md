# Hyve API

Making this myself bc the other one was *bad*

For documentation, run `yarn doc` and it'll trigger tsdoc to *hopefully*
generate all the documentation I wrote in the code.

To start the API, `yarn build && yarn start`

For developing, `yarn dev`

Linter is XO cause it has a cool name and looks pretty. `yarn lint` will automatically fix all
your formatting errors
