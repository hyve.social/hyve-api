const profileLinkGenerator = (username: string) =>
	`${require('process').env.PROFILE_LINK_HEAD + require('process').env.PORT}/${username}`;
export default profileLinkGenerator;
