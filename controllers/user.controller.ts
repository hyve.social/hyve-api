import {Router, Request, Response, NextFunction} from 'express';
import validate from '@/validations/user.validator';
import Controller from '@/interfaces/controller.interface';
import HttpException from '@/utils/exceptions/http.exception';
import validationMiddleware from '@/middleware/validation.middleware';
import UserRoute from '../routes/user.route';
import authenticated from '@/middleware/authenticated.middleware';

/**
 * The base UserController that will define all the behaviours for what the user can do.
 * TODO: Document this later. I'm tired.
 * @implements Controller
 */
class UserController implements Controller {
	public path = '/users';
	public router = Router();
	private readonly UserService = new UserRoute();

	constructor() {
		this.initialiseRoutes();
	}

	private initialiseRoutes(): void {
		this.router.post(
			`${this.path}/register`,
			validationMiddleware(validate.register),
			this.register,
		);
		this.router.post(
			`${this.path}/login`,
			validationMiddleware(validate.login),
			this.login,
		);
		this.router.get(`${this.path}`, authenticated, this.getUser);
	}

	private readonly register = async (
		request: Request,
		response: Response,
		next: NextFunction,
	): Promise<Response | void> => {
		try {
			const {username, email, password} = request.body;

			const token = await this.UserService.register(
				username,
				email,
				password,
				'user',
			);

			response.status(201).json({token});
		} catch (error: any) {
			next(new HttpException(400, error.message));
		}
	};

	private readonly login = async (
		request: Request,
		response: Response,
		next: NextFunction,
	): Promise<Response | void> => {
		try {
			const {email, password} = request.body;

			const token = await this.UserService.login(email, password);

			response.status(200).json({token});
		} catch (error: any) {
			next(new HttpException(400, error.message));
		}
	};

	private readonly getUser = (
		request: Request,
		response: Response,
		next: NextFunction,
	): Response | void => {
		if (!request.user) {
			next(new HttpException(404, 'No logged in user'));
			return;
		}

		response.status(200).send({data: request.user});
	};
}

export default UserController;
