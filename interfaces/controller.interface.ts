import {Router} from 'express';
/**
 * Default Interface for all API Controllers. This is done to enforce a path &
 * router, so we can mitigate runtime errors of traits within controllers being undefined.
 * @param path {string} - What path the controller will be listening on.
 * @param router {Router} - Express router.
 */
export default interface Controller {
	path: string;
	router: Router;
}
