import {Schema} from 'mongoose';
/** Our main interface for handling JWT Tokens. */
export default interface TokenInterface extends Object {
	id: Schema.Types.ObjectId;
	expiresIn: number;
}
