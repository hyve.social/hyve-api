import {Document} from 'mongoose';
// I really don't think this needs explaining.
export default interface UserInterface extends Document {
	username: string;
	email: string;
	password: string;
	role: string;
	isValidPassword(password: string): Promise<Error | boolean>;
}
