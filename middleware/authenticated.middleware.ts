import {Request, Response, NextFunction} from 'express';
import jwt from 'jsonwebtoken';
import token from '@/utils/token';
import UserModel from '@/models/user.model';
import Token from '@/interfaces/token.interface';
import HttpException from '@/utils/exceptions/http.exception';

async function authenticatedMiddleware(
	request: Request,
	res: Response,
	next: NextFunction,
): Promise<Response | void> {
	const bearer = request.headers.authorization;

	if (!bearer || !bearer.startsWith('Bearer ')) {
		next(new HttpException(401, 'Unauthorised'));
		return;
	}

	const accessToken = bearer.split('Bearer ')[1].trim();
	try {
		const payload: Token | jwt.JsonWebTokenError = await token.verifyToken(
			accessToken,
		);

		if (payload instanceof jwt.JsonWebTokenError) {
			next(new HttpException(401, 'Unauthorised'));
			return;
		}

		const user = await UserModel.findById(payload.id)
			.select('-password')
			.exec();

		if (!user) {
			next(new HttpException(401, 'Unauthorised'));
			return;
		}

		request.user = user;

		next(); return;
	} catch {
		next(new HttpException(401, 'Unauthorised'));
	}
}

export default authenticatedMiddleware;
