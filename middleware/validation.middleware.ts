import {Request, Response, NextFunction} from 'express';
import Joi from 'joi';

/**
 * When requests are made to our API, we want to sanitise our output by validating it against the Schema.
 * For this, we use the `Joi` library; we can check whether the data in the request lines up with what we're
 * expecting, and if not, we can simply just send an error code.
 * @param schema
 * @returns
 */
const validateMiddleware = (schema: Joi.Schema) =>
	async (
		request: Request,
		response: Response,
		next: NextFunction,
	): Promise<void> => {
		const validationOptions = {
			/** If we don't do `abortEarly`, on the first validation failure all the errors will be returned back to the user.
       * So if we have a login form where they didn't pass a username & password; it will tell them specifically *all* the
       * errors they made. Which is a lot more useful rather than getting one errror back at a time, right? :>
       */
			abortEarly: false,
			/** This allows values that *aren't* part of the schema. We don't want our API to fail pointlessly, at least not at this stage.
       */
			allowUnknown: true,
			/** With this option, we're now *stripping away* the excess values that might get returned. This is also extremely useful from
       * a security standpoint; we don't want threat actors injecting whatever random headers they like and possibly getting things from our database.
       */
			stripUnknown: true,
		};

		try {
			const value = await schema.validateAsync(
				request.body, validationOptions,
			);
			request.body = value;
			// Continue with the request
			next();
		} catch (error: any) { // Oh god prepare for error err errors hell.
			const errors: string[] = [];
			error.details.forEach((error_: Joi.ValidationError) => {
				errors.push(error_.message);
			});
			response.status(400).send({errors});
		}
	};

export default validateMiddleware;

