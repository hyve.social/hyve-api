import {Schema, model} from 'mongoose';
import bcrypt from 'bcrypt';
import UserInterface from 'interfaces/user.interface';

/**
 * `UserModel` is essentially the mongoose schema for our user. Shocking I know.
 */
const UserModel = new Schema({
	username: {
		type: String,
		required: true,
		unique: true,
		min: 4,
		max: 30,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	bio: {
		type: String,
		max: 150,
		default: 'Welcome to my hyve!',
	},
	password: {
		type: String,
		required: true,
	},
	userLinks: [{
		linkName: String,
		linkBody: String,
	}],
	profileLink: String,
},
{timestamps: true},
);

/**
 * In order to like. Not store the user's passwords in plaintext, let's salt & hash it first before we
 * upload it to the database, yeah?
 */
UserModel.pre<UserInterface>('save', async function (next) {
	if (!this.isModified('password')) {
		next();
		return;
	}

	const hash = await bcrypt.hash(this.password, 10);
	this.password = hash;
	next();
});

/**
 * Don't let the user just change to the same / similar password. Kinda pointless isn't it :p
 * @param password {string} - The user's password that they submit.
 */
UserModel.methods.isValidPassword = async function (password: string): Promise<Error | boolean> {
	return bcrypt.compare(password, this.password);
};

export default model<UserInterface>('UserModel', UserModel);
