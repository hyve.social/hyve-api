import UserModel from '@/models/user.model';
import token from '@/utils/token';

type ApiHttpResponse = Promise<string | Error>;

export default class UserRoute {
	private readonly user = UserModel;

	/**
   * Register a new user! At last.
   */
	public async register(
    username: string,
    email: string,
    password: string,
    role: string,
  ): ApiHttpResponse {
		try {
			const user = await this.user.create({
				username,
				email,
				password,
				role,
			});
	
			const accessToken = token.createToken(user);
			return accessToken;
		} catch (error: any) {
			throw new Error(error.message);
		}
  }

	/**
   * Let's try to actually log them in too.
   * Idk what do you actually expect me to say to document this,
   * "Ah yes, you see when we run the login function we make a request to the API
   * to check the user exists within the database, then if their credentials they
   * entered are valid and then we assign then a JWT token to stay logged in for
   * however long until they clear the cookie"
   * Like idk it's a login function what do you think it does.
   * @returns Promise<string | Error>
   */
	public async login(email: string, password: string): ApiHttpResponse {
		try {
			const user = await this.user.findOne({email});
			if (!user) {
				throw new Error('User does not exist! Or you typed it in wrong. Or this API is horribly broken 0____0');
			}

			if (await user.isValidPassword(password)) {
				return token.createToken(user);
			}

			throw new Error('Wrong credentials given.');
		} catch (error: any) {
			throw new Error(`Unable to create the user!, \n ${error}`);
		}
	}
}
