import 'dotenv/config';
import 'module-alias/register';
import Hyve from './Hyve';
import validateEnv from '@/utils/validateEnv';
import UserController from './controllers/user.controller';

validateEnv();
const hyve = new Hyve([new UserController()], Number(require('process').env.PORT));

hyve.listen();

export default hyve;