import jwt from 'jsonwebtoken';
import UserInterface from '@/interfaces/user.interface';
import TokenInterface from '@/interfaces/token.interface';

export const createToken = (user: UserInterface): string =>
	jwt.sign({id: user._id}, require('process').env.JWT_SECRET as jwt.Secret, {
		expiresIn: '1d',
	});

export const verifyToken = async (token: string): Promise<jwt.VerifyErrors | TokenInterface> =>
	new Promise((resolve, reject) => {
		jwt.verify(token, process.env.JWT_SECRET as jwt.Secret, (error: any, payload) => {
			if (error) {
				reject(error);
				return;
			}

			resolve(payload as TokenInterface);
		});
	});

export default {createToken, verifyToken};
