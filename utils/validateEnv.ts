import {cleanEnv, str, port} from 'envalid';
/**
 * `validateEnv` is a function used to validate and sanitise the dotenv configuration.
 * It allows us to set defaults for options, choose whether we want to run our app
 * in production or development mode, ensure we've set all the correct options, etc etc.
 * This is also insanely useful for when this grows in complexity, because *oh god will it grow.*
 */
const validateEnv = (): void => {
	cleanEnv(require('process').env, {
		NODE_ENV: str({choices: ['development', 'production']}),
		MONGODB_PASSWORD: str(),
		MONGODB_USER: str(),
		PORT: port({default: 8000}),
	});
};

export default validateEnv;
