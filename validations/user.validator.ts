import Joi from 'joi';

const password = Joi.string().min(6).required();
const email = Joi.string().email().required();

const authenticated = Joi.object({
	email,
	password: Joi.string().min(6).required(),
});

const login = Joi.object({
	email: Joi.string().email().required(),
	password,
});

const register = Joi.object({
	username: Joi.string().required(),
	email: Joi.string().email().required(),
	password,
});

export default {login, register, authenticated};
